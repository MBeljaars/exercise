package com.BarChart;

import java.sql.SQLException;
import java.util.ArrayList;

import com.Database;
import com.Nodes.NamedNode;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;

// Extends so we dont need to instantiate a db again
public class FietstrommelsStraat extends BarChartSample {
    private ArrayList<NamedNode> BlackList;

    //Constructor: run query and save it in private list for future reference
    public FietstrommelsStraat(Database dbin) throws SQLException{
        db = dbin;
        String query ="SELECT fietstrommels.fiestrommel_locatie,COUNT(fietstrommels.fietstrommel_key) FROM fietstrommels GROUP BY fietstrommels.fiestrommel_locatie ORDER BY COUNT(fietstrommels.fietstrommel_key) DESC";
        db.rs = db.stmt.executeQuery(query);
        BlackList = new ArrayList<>();
        int index = 0;
        while(db.rs.next()){
            BlackList.add(new NamedNode(index,db.rs.getString("fiestrommel_locatie"),db.rs.getInt("COUNT(fietstrommels.fietstrommel_key)")));
            index++;
        }
    }

    // We use the method showChart here to pass the values we want to display in this specific barChart
    public void showChart() {
        Stage stage = new Stage();
        stage.setTitle("Straten met meeste fietstrommels");
        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();
        final BarChart<String,Number> bc =
                new BarChart<String,Number>(xAxis,yAxis);
        bc.setTitle("Straten met meeste fietstrommels");
        xAxis.setLabel("Straatnaam");
        yAxis.setLabel("Aantal fietstrommels");
        XYChart.Series series1 = new XYChart.Series();

        for (NamedNode values: this.BlackList){

            if (values.GetIndex() < 10){
                series1.getData().add(new XYChart.Data(values.GetName(), values.GetValue()));
            }

        }

        Scene scene  = new Scene(bc,800,600);
        bc.getData().add(series1);
        stage.setScene(scene);
        stage.show();
    }

}