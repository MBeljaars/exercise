package com.BarChart;

import com.Database;
import com.Difference;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;
import java.sql.SQLException;

//Extends so we dont need to instantiate a db again
public class Fietsdiefstal_trommel_verband extends BarChartSample{

    private float totalThefts_with_trommel;
    private float allThefts;

    //Constructor: run query and save it in private list for future reference
    public Fietsdiefstal_trommel_verband(Database input) throws SQLException{
        db = input;
        Difference diefstal_trommel_verband = new Difference(db);
        totalThefts_with_trommel = diefstal_trommel_verband.TheftTrom();
        allThefts = diefstal_trommel_verband.TotalTheft();
    }

    //We use the method showChart here to pass the values we want to display in this specific barChart
    public void showChart() {
        Stage stage = new Stage();
        stage.setTitle("Verband fietstrommels en fietsdiefstallen");
        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();
        final BarChart<String, Number> bc =
                new BarChart<String, Number>(xAxis, yAxis);
        XYChart.Series series1 = new XYChart.Series();
        series1.getData().add(new XYChart.Data("Straat met trommel", totalThefts_with_trommel));
        XYChart.Series series2 = new XYChart.Series();
        series2.getData().add(new XYChart.Data("Straat zonder trommel", allThefts));

        yAxis.setLabel("Aantal fietsdiefstallen");
        xAxis.setLabel("Soort straat");
        Scene scene  = new Scene(bc,800,600);
        series1.setName("straat met trommel");
        series2.setName("straat zonder trommel");
        bc.getData().addAll(series1, series2);
        stage.setScene(scene);
        stage.show();
    }
}