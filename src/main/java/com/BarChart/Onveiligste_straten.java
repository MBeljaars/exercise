package com.BarChart;

import java.sql.SQLException;
import java.util.ArrayList;

import com.Database;
import com.Nodes.NamedNode;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;

//Extends so we dont need to instantiate a db again
public class Onveiligste_straten extends BarChartSample {

    private ArrayList<NamedNode> BlackList;
    private ArrayList<NamedNode> Lijst2011;
    private ArrayList<NamedNode> Lijst2012;
    private ArrayList<NamedNode> Lijst2013;

    //A query in which all the thefts from each year are combined
    public Onveiligste_straten(Database dbin) throws SQLException{
        db = dbin;
        String query ="SELECT diefstal_straat,COUNT(diefstal_straat) FROM `diefstal` GROUP BY diefstal_straat ORDER BY COUNT(diefstal_straat) DESC";
        db.rs = db.stmt.executeQuery(query);
        BlackList = new ArrayList<>();
        int index = 0;
        while(db.rs.next()){
            BlackList.add(new NamedNode(index,db.rs.getString("diefstal_straat"),db.rs.getInt("COUNT(diefstal_straat)")));
            index++;
        }

        //A specific query for each year so we can use that result to show in a graph specific to that year
        query ="SELECT diefstal_straat,COUNT(diefstal_straat) FROM `diefstal` WHERE diefstal.diefstal_gemjaar = 2011 GROUP BY diefstal_straat ORDER BY COUNT(diefstal_straat) DESC";
        db.rs = db.stmt.executeQuery(query);
        Lijst2011 = new ArrayList<>();
        index = 0;
        while(db.rs.next()){
            Lijst2011.add(new NamedNode(index,db.rs.getString("diefstal_straat"),db.rs.getInt("COUNT(diefstal_straat)")));
            index++;
        }

        //A specific query for each year so we can use that result to show in a graph specific to that year
        query ="SELECT diefstal_straat,COUNT(diefstal_straat) FROM `diefstal` WHERE diefstal.diefstal_gemjaar = 2012 GROUP BY diefstal_straat ORDER BY COUNT(diefstal_straat) DESC";
        db.rs = db.stmt.executeQuery(query);
        Lijst2012 = new ArrayList<>();
        index = 0;
        while(db.rs.next()){
            Lijst2012.add(new NamedNode(index,db.rs.getString("diefstal_straat"),db.rs.getInt("COUNT(diefstal_straat)")));
            index++;
        }

        //A specific query for each year so we can use that result to show in a graph specific to that year
        query ="SELECT diefstal_straat,COUNT(diefstal_straat) FROM `diefstal` WHERE diefstal.diefstal_gemjaar = 2013 GROUP BY diefstal_straat ORDER BY COUNT(diefstal_straat) DESC";
        db.rs = db.stmt.executeQuery(query);
        Lijst2013 = new ArrayList<>();
        index = 0;
        while(db.rs.next()){
            Lijst2013.add(new NamedNode(index,db.rs.getString("diefstal_straat"),db.rs.getInt("COUNT(diefstal_straat)")));
            index++;
        }
    }


    // We use the method showChart here to pass the values we want to display in this specific barChart
    public void showChart(int inputjaar) {
        //Here we take care of the choice of the user so he/she can choose between
        if(inputjaar == 0){
            Stage stage = new Stage();
            stage.setTitle("Fietsdiefstal in jaren");
            final CategoryAxis xAxis = new CategoryAxis();
            final NumberAxis yAxis = new NumberAxis();
            final BarChart<String,Number> bc =
                    new BarChart<String,Number>(xAxis,yAxis);
            bc.setTitle("Straten met meeste fietsdiefstallen");
            xAxis.setLabel("Straatnaam");
            yAxis.setLabel("Aantal fietsdiefstallen");

            XYChart.Series series1 = new XYChart.Series();
            XYChart.Series series2 = new XYChart.Series();
            XYChart.Series series3 = new XYChart.Series();
            XYChart.Series series4 = new XYChart.Series();
            for (NamedNode values: this.BlackList){
                if (values.GetIndex() < 10){
                    series1.getData().add(new XYChart.Data(values.GetName(), values.GetValue()));

                    for (NamedNode valueslol: this.Lijst2011){
                        if (valueslol.GetName().equals(values.GetName())) {
                            series2.getData().add(new XYChart.Data(valueslol.GetName(), valueslol.GetValue()));
                        }
                    }
                    for (NamedNode valueslol: this.Lijst2012){
                        if (valueslol.GetName().equals(values.GetName())) {
                            series3.getData().add(new XYChart.Data(valueslol.GetName(), valueslol.GetValue()));
                        }
                    }
                    for (NamedNode valueslol: this.Lijst2013){
                        if (valueslol.GetName().equals(values.GetName())) {
                            series4.getData().add(new XYChart.Data(valueslol.GetName(), valueslol.GetValue()));
                        }
                    }

                }
            }


            Scene scene  = new Scene(bc,800,600);
            series1.setName("Totaal");
            series2.setName("2011");
            series3.setName("2012");
            series4.setName("2013");
            bc.getData().addAll(series1,series2,series3,series4);
            bc.setLayoutX(50);
            stage.setScene(scene);
            stage.show();
        }

        //Here we handle if a user chooses one of the individual years
        else{
            Stage stage = new Stage();
            stage.setTitle("Fietsdiefstal in het jaar " + inputjaar);
            final CategoryAxis xAxis = new CategoryAxis();
            final NumberAxis yAxis = new NumberAxis();
            final BarChart<String,Number> bc =
                    new BarChart<String,Number>(xAxis,yAxis);
            bc.setTitle("Straten met meeste fietsdiefstallen in "+ inputjaar);
            xAxis.setLabel("Straatnaam");
            yAxis.setLabel("Aantal fietsdiefstallen");

            XYChart.Series series1 = new XYChart.Series();

            if(inputjaar == 2011){
            for (NamedNode values: Lijst2011){
                if (values.GetIndex() < 10){
                    series1.getData().add(new XYChart.Data(values.GetName(), values.GetValue()));
                }
                }
            }
            if(inputjaar == 2012){
                for (NamedNode values: Lijst2012){
                    if (values.GetIndex() < 10){
                        series1.getData().add(new XYChart.Data(values.GetName(), values.GetValue()));
                    }
                }
            }
            if(inputjaar == 2013){
                for (NamedNode values: Lijst2013){
                    if (values.GetIndex() < 10){
                        series1.getData().add(new XYChart.Data(values.GetName(), values.GetValue()));
                    }
                }
            }
            Scene scene  = new Scene(bc,800,600);
            series1.setName("Aantal fietsdiefstallen");
            bc.getData().addAll(series1);
            bc.setLayoutX(50);
            stage.setScene(scene);
            stage.show();
        }
    }
}
