package com.BarChart;

import com.Database;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;

// This serves as a superclass for all the barcharts so we wont have to initiate a db in all barcharts
abstract class BarChartSample {

    protected Database db;

    Stage stage = new Stage();
    Scene scene = new Scene(new Group());

    public BarChartSample(){
        stage.setTitle("BarChart Data");
        stage.setWidth(500);
        stage.setHeight(500);
    }

    // Incase we want to set the title of a string through a method
    public void setTitle(String title) {
        stage.setTitle(title);
    }
}