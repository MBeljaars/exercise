package com.Nodes;
//ValueNodes are used to save 2 values that are connected with each other, for example a street and it s value.
public class NamedNode {
    //attributes are created; index is the position of this Node in a list of NamedNodes.
    private int index;
    private String Name;
    private int Value;
    public NamedNode(int index,String NameIn, int ValueIn){
        this.index = index;
        Name = NameIn;
        Value = ValueIn;
    }
    //To return each attribute
    public String GetName(){
        return Name;
    }
    public int GetValue(){
        return Value;
    }
    public int GetIndex(){
        return index;
    }
}