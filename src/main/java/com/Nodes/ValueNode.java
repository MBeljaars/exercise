package com.Nodes;
//ValueNodes are used to save 2 values that are connected with each other, for example a year and it s value.
public class ValueNode {
    //attributes are created.
    private int Point;
    private int Value;
    public ValueNode(int YearIn, int ValueIn){
        Point = YearIn;
        Value = ValueIn;
    }
    //to return each attribute
    public int GetPoint(){
        return Point;
    }
    public int GetValue(){
        return Value;
    }
}