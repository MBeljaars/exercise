package com.PieChart;

import com.Database;
import com.Nodes.NamedNode;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.chart.PieChart;
import java.sql.SQLException;
import java.util.*;

public class Merk_Fietsen extends PieChartSample {
    private Database db;
    ArrayList<NamedNode> fietsMerkLijst;

    //Constructor: run query and save it in private list for future reference
    public Merk_Fietsen(Database dbin) throws SQLException{

        db = dbin;
        String query = "select diefstal_merk,count(diefstal_voorvalnummer) from diefstal where diefstal_merk <> '0' group by diefstal_merk";
        db.rs = db.stmt.executeQuery(query);
        fietsMerkLijst = new ArrayList<>();
        int index = 0;
        while (db.rs.next()) {
            index ++;
            fietsMerkLijst.add(new NamedNode(index, db.rs.getString("diefstal_merk"), db.rs.getInt("count(diefstal_voorvalnummer)")));
        }
    }

    // We use the method showChart here to pass the values we want to display in this specific pieChart
    public void showChart(){
        ObservableList<PieChart.Data> pieChartData =
                FXCollections.observableArrayList(
                        );

        for (NamedNode values:fietsMerkLijst){
            if ((!values.GetName().equals("ONBEKEND"))&&(values.GetValue() > 400)) {
                pieChartData.add(new PieChart.Data(values.GetName(), values.GetValue()));
            }
        }
        final PieChart chart = new PieChart(pieChartData);
        ((Group) scene.getRoot()).getChildren().add(chart);
        stage.setScene(scene);
        stage.setTitle("Fietsdiefstal merken");
        chart.setTitle("Merk gestolen fietsen omgeving Rotterdam");
        stage.show();
    }
}