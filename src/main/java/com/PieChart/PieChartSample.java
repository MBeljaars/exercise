package com.PieChart;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.*;
import javafx.stage.Stage;
import javafx.scene.chart.*;
import javafx.scene.Group;

//We create a superclass PieChartSample so we dont have to pass some variabeles in subclasses later
abstract class PieChartSample {

    Stage stage = new Stage();
    Scene scene = new Scene(new Group());

    public PieChartSample(){
        stage.setTitle("Piechart Data");
        stage.setWidth(500);
        stage.setHeight(500);
    }

    public void setTitle(String title) {
        stage.setTitle(title);
    }

}



