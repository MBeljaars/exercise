package com.PieChart;

import com.Database;
import com.Nodes.NamedNode;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.chart.PieChart;
import java.sql.SQLException;
import java.util.*;


public class Kleur_Fietsen extends PieChartSample {
    private Database db;
    ArrayList<NamedNode> DiefTijdLijst;
    //Constructor: run query and save it in private list for future reference
    public Kleur_Fietsen(Database dbin) throws SQLException {
        db = dbin;

        String query = "select diefstal_kleur,count(diefstal_voorvalnummer) from diefstal group by diefstal_kleur";
        db.rs = db.stmt.executeQuery(query);
        DiefTijdLijst = new ArrayList<>();
        int index = 0;
        while (db.rs.next()) {
            index++;
            DiefTijdLijst.add(new NamedNode(index, db.rs.getString("diefstal_kleur"), db.rs.getInt("count(diefstal_voorvalnummer)")));
        }
    }

    //We use the method showChart here to pass the values we want to display in this specific pieChart
    public void showChart() {
        ObservableList<PieChart.Data> pieChartData =
                FXCollections.observableArrayList(
                );

        for (NamedNode values : DiefTijdLijst) {
            if ((!values.GetName().equals("ONBEKEND")) && (values.GetValue() > 400)) {
                pieChartData.add(new PieChart.Data(values.GetName(), values.GetValue()));
            }
        }

        final PieChart chart = new PieChart(pieChartData);
        ((Group) scene.getRoot()).getChildren().add(chart);
        stage.setScene(scene);
        stage.setTitle("Fietsdiefstal kleuren");
        chart.setTitle("Kleur gestolen fietsen omgeving Rotterdam");
        scene.getStylesheets().add("piechart_css.css");
        stage.show();

    }
}

