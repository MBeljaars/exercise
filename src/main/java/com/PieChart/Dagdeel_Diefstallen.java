package com.PieChart;

import com.Database;
import com.Nodes.ValueNode;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.chart.PieChart;
import java.sql.SQLException;
import java.util.ArrayList;

// We instantiate a subclass of PieChartSample so we can modify its values here
public class Dagdeel_Diefstallen extends PieChartSample {
    private Database db;
    ArrayList<ValueNode> DiefTijdLijst;
    int NightValue;
    int MorningValue;
    int NounValue;
    int EveningValue;

    String NightString;
    String MorningString;
    String NounString;
    String EveningString;

    // Constructor: run query and save it in private list for future reference
    public Dagdeel_Diefstallen(Database dbin) throws SQLException {
        db = dbin;

        // We assign the specific query for this Piechart to a variable so we can use it on our database through the method executeQuery
        String query = "select diefstal_gemdagdeel,count(diefstal_gemdagdeel) from diefstal group by diefstal_gemdagdeel";
        db.rs = db.stmt.executeQuery(query);
        DiefTijdLijst = new ArrayList<>();

        // We add the results of the resultsets in Nodes to the list
        while (db.rs.next()) {
            DiefTijdLijst.add(new ValueNode(db.rs.getInt("diefstal_gemdagdeel"), db.rs.getInt("COUNT(diefstal_gemdagdeel)")));
        }

        NightValue = DiefTijdLijst.get(0).GetValue();
        MorningValue = DiefTijdLijst.get(1).GetValue();
        NounValue = DiefTijdLijst.get(2).GetValue();
        EveningValue = DiefTijdLijst.get(3).GetValue();
        NightString = "'s Nachts";
        MorningString = "'s Ochtends";
        NounString = "'s Middags";
        EveningString = "'s Avonds";

    }

    // We use the method showChart here to pass the values we want to display in this specific pieChart
    public void showChart(){
        ObservableList<PieChart.Data> pieChartData =
                FXCollections.observableArrayList(
                        new PieChart.Data(NightString, NightValue),
                        new PieChart.Data(MorningString, MorningValue),
                        new PieChart.Data(NounString, NounValue),
                        new PieChart.Data(EveningString, EveningValue));
        final PieChart chart = new PieChart(pieChartData);
        ((Group) scene.getRoot()).getChildren().add(chart);
        stage.setScene(scene);
        stage.setTitle("Dagdeel fietsdiefstallen");
        chart.setTitle("Fietsdiefstallen per dagdeel omgeving Rotterdam");
        stage.show();
    }
}

