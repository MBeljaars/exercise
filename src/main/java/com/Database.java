package com;

import java.sql.*;
import java.util.ArrayList;

//Database is a class that is used to maintain the connection between java application and our database.
public class  Database {

    public Connection conn = null;
    public Statement stmt = null;
    public ResultSet rs = null;

    //constructor attemps to establish a connection with the database.
    public Database(String url, String user, String pass) {
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
