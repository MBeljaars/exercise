package com.Menus;

import com.BarChart.Onveiligste_straten;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
//This class is meant as a sub-menu, that allows the user to chose between monthy and yearly graph.
public class Onveiligste_strateMENU {

    Stage stage;

    public Onveiligste_strateMENU(){
    }
    //When this method is triggered; it will open a new window with new menu on stage
    //This menu will allows the user to chose what year they d like to show the data from.
    public void showChart(Onveiligste_straten input){
        Scene scene;
        //A new stage is created to create the pop up.
        stage = new Stage();
        stage.setTitle("Groei Diefstal");


        // The popup scene is filled with buttons
        Button quitButton = new Button("Quit");

        ChoiceBox<String> choiceBox = new ChoiceBox<>();
        choiceBox.getItems().add("2011");
        choiceBox.getItems().add("2012");
        choiceBox.getItems().add("2013");
        choiceBox.getItems().add("Totaal");
        choiceBox.getItems().add("Kies jaar");
        choiceBox.setValue("Kies jaar");

        //The options in choicebox get their operations added
        choiceBox.setOnAction(event ->
        {
            if (choiceBox.getValue().equals("2011")) {
                input.showChart(2011);
            }
            if (choiceBox.getValue().equals("2012")) {
                input.showChart(2012);
            }
            if (choiceBox.getValue().equals("2013")) {
                input.showChart(2013);
            }
            if (choiceBox.getValue().equals("Totaal")) {
                input.showChart(0);
            }
        });
        quitButton.setOnAction( event -> closeProgram());

        // Popup is sized and get s it s style set.
        VBox layout = new VBox(5);
        layout.setAlignment(Pos.CENTER);
        layout.getChildren().addAll(choiceBox,quitButton);
        scene = new Scene(layout, 300, 200);
        scene.getStylesheets().add("com/css.css");

        scene.getStylesheets().add("com/css.css");
        stage.setScene(scene);
        stage.show();
        quitButton.setOnAction( event -> closeProgram());
    }
    //CLose the popup
    public void closeProgram() {
        stage.close();
    }
}

