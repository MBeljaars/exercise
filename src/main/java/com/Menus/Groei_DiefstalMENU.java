package com.Menus;

import com.LineChart.Jaarlijkse_diefstallen;
import com.LineChart.Maandelijkse_diefstallen;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

//This class is meant as a sub-menu, that allows the user to chose between monthy and yearly graph.
public class Groei_DiefstalMENU {

    Stage stage;

    public Groei_DiefstalMENU(){
    }
    //When this method is triggered; it will open a new window with new menu on stage
    //This menu will allows the user to chose what year they d like to show the data from.
    public void showChart(Jaarlijkse_diefstallen JD, Maandelijkse_diefstallen MD){
        Scene scene;
        //A new stage is created to create the pop up.
        stage = new Stage();
        stage.setTitle("Groei Diefstal");

        // The popup scene is filled with buttons
        Button quitButton = new Button("Quit");
        ChoiceBox<String> choiceBox = new ChoiceBox<>();
        choiceBox.getItems().add("Aantal diefstallen per jaar");
        choiceBox.getItems().add("Aantal diefstallen per maand");
        choiceBox.getItems().add("Kies grafiek");
        choiceBox.setValue("Kies grafiek");

        //The options in choicebox get their operations added
        choiceBox.setOnAction(event ->
        {
            if (choiceBox.getValue().equals("Aantal diefstallen per jaar")) {
                JD.showChart();
            }
            if (choiceBox.getValue().equals("Aantal diefstallen per maand")) {
                MD.showChart();
            }
        });
        quitButton.setOnAction( event -> closeProgram());

        // Popup is sized and get s it s style set.
        VBox layout = new VBox(5);
        layout.setAlignment(Pos.CENTER);
        layout.getChildren().addAll(choiceBox,quitButton);
        scene = new Scene(layout, 300, 200);
        scene.getStylesheets().add("com/css.css");
        stage.setScene(scene);
        stage.show();

    }

    public void closeProgram() {
        stage.close();
    }
}

