package com;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;


public class ConfirmBox {

    static boolean answer;

    public static boolean display(String title, String message){
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.setMinWidth(600);
        Label label = new Label();
        label.setText(message);

        Button yesButton = new Button("Yes");
        Button noButton = new Button("No");

        //Events are used to give the buttons specific actions
        yesButton.setOnAction(event ->{
            answer = true;
            window.close();
        });

        noButton.setOnAction(event ->{
            answer = false;
            window.close();
        });

        VBox layout = new VBox(10);
        //Here we get the text and buttons to show them on the confirmbox
        layout.getChildren().addAll(label, yesButton, noButton);
        layout.setAlignment(Pos.CENTER);
        Scene scene = new Scene(layout);
        //We wanted to include the same css
        scene.getStylesheets().add("com/css.css");
        window.setScene(scene);
        window.showAndWait();
        return answer;
    }
}