package com;

import com.BarChart.Fietsdiefstal_trommel_verband;
import com.BarChart.FietstrommelsStraat;
import com.BarChart.Onveiligste_straten;
import com.LineChart.Jaarlijkse_diefstallen;
import com.LineChart.Maandelijkse_diefstallen;
import com.Menus.Groei_DiefstalMENU;
import com.Menus.Onveiligste_strateMENU;
import com.PieChart.Dagdeel_Diefstallen;
import com.PieChart.Kleur_Fietsen;
import com.PieChart.Merk_Fietsen;
import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.image.Image;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;


public class GUIMain extends Application {
    //window and scene and database are declared.
    Stage window;
    Scene scene;
    Database DB;

    //All instances of the classes are declared to be used later on.
    Jaarlijkse_diefstallen gr;
    Dagdeel_Diefstallen dt;
    Onveiligste_straten md;
    Kleur_Fietsen fk;
    Merk_Fietsen fm;
    FietstrommelsStraat fs;
    Fietsdiefstal_trommel_verband rp;
    Maandelijkse_diefstallen mdd;
    Groei_DiefstalMENU groeiframe;
    Onveiligste_strateMENU straatframe;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws SQLException, IOException {
        //window gains it value
        window = primaryStage;
        //Initializes the database class to establish connection.
        String url = "jdbc:mysql://localhost/rotterdamdata";
        String username = "root";
        String password = "";
        DB = new Database(url, username, password);

        //All the classes are initialized; their querries are run and all we have to do to show a graph is
        //call their .display() method and a new window will be opened.
        gr = new Jaarlijkse_diefstallen(DB);
        dt = new Dagdeel_Diefstallen(DB);
        mdd = new Maandelijkse_diefstallen(DB);
        fk = new Kleur_Fietsen(DB);
        fm = new Merk_Fietsen(DB);
        rp = new Fietsdiefstal_trommel_verband(DB);
        md = new Onveiligste_straten(DB);
        fs = new FietstrommelsStraat(DB);

        //The sub menu class is initialized. Opens sub menus that allows users to chose extra parameters.
        groeiframe = new Groei_DiefstalMENU();
        straatframe = new Onveiligste_strateMENU();
        Button closeButton = new Button("Quit");
        closeButton.setOnAction(e -> {
            closeProgram();
        });

        //QuitButton
        Button quitButton = new Button("Quit");
        quitButton.setOnAction(event -> closeProgram());

        //Adding new choices to the choicebox, which options there are to choose.
        ChoiceBox<String> choiceBox = new ChoiceBox<>();
        choiceBox.getItems().add("Diefstallen per dagdeel");
        choiceBox.getItems().add("Diefstal per straat");
        choiceBox.getItems().add("Groei diefstal in de afgelopen jaren");
        choiceBox.getItems().add("Kleur van gestolen fietsen");
        choiceBox.getItems().add("Merk van gestolen fietsen");
        choiceBox.getItems().add("Straten met meeste fietstrommel");
        choiceBox.getItems().add("Verband fietstrommels fietsdiefstallen");
        choiceBox.getItems().add("Kies grafiek");
        choiceBox.setValue("Kies grafiek");

        // Window Close
        window.setOnCloseRequest(event -> {
            event.consume();
            closeProgram();
        });
        // Adds functions to the options of the choiceboxes.  .showChart() method opens a new pop up window. Usually showing a chart
        // or a new dropbox menu to allow the user choose extra options.
        choiceBox.setOnAction(event ->
                {
                    if (choiceBox.getValue().equals("Groei diefstal in de afgelopen jaren")) {
                        groeiframe.showChart(gr,mdd);
                    }
                    if (choiceBox.getValue().equals("Diefstal per straat")) {
                        straatframe.showChart(md);
                    }
                    if (choiceBox.getValue().equals("Diefstallen per dagdeel")) {
                        dt.showChart();
                    }
                    if (choiceBox.getValue().equals("Kleur van gestolen fietsen")) {
                        fk.showChart();
                    }
                    if (choiceBox.getValue().equals("Merk van gestolen fietsen")){
                        fm.showChart();
                    }
                    if (choiceBox.getValue().equals("Verband fietstrommels fietsdiefstallen")){
                        rp.showChart();
                    }
                    if (choiceBox.getValue().equals("Straten met meeste fietstrommel")){
                        fs.showChart();
                    }
                });

        // Layout & Gridpane
        GridPane gridpane = new GridPane();
        gridpane.setPadding(new Insets(0));
        gridpane.setVgap(10);
        gridpane.setHgap(10);
        GridPane.setConstraints(choiceBox, 0, 0);
        BufferedImage skyline = ImageIO.read(new File("image_Skyline.png"));
        Image memes = SwingFXUtils.toFXImage(skyline, null);
        ImageView skyline_image = new ImageView(memes);
        VBox layout = new VBox(20);
        //center layout to keep everything in the middle
        layout.setAlignment(Pos.CENTER);
        layout.getChildren().addAll(choiceBox,closeButton,skyline_image);
        scene = new Scene(layout, 900, 500);
        scene.getStylesheets().add("com/css.css");

        window.setScene(scene);
        window.setTitle("Javatars");
        window.show();
    }
    //method that is ran when a quit button is pressed. Opens the ConfirmBox class.
    public void closeProgram() {
        boolean answer = ConfirmBox.display("Are you sure?", "Are you sure you want to close?");
        if (answer) {
            window.close();
        }
    }
}