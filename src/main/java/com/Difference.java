package com;

import com.Nodes.ValueNode;

import java.sql.SQLException;
import java.util.ArrayList;

public class Difference {
    private Database db;
    ArrayList<ValueNode> PercentageLijst;
    public Difference(Database dbin) throws SQLException{
        db = dbin;

        PercentageLijst = new ArrayList<>();
    }

    //Alle diefstallen met een trommel in de straat
    //Dit gebruiken we om te dalijk uit te kunnen rekenen wat het percentage diefstallen bij fietstrommels is
    public float TheftTrom() throws SQLException{
        String query ="SELECT count(distinct diefstal.diefstal_voorvalnummer) FROM diefstal, fietstrommels WHERE diefstal.diefstal_straat = fietstrommels.fiestrommel_locatie";
        db.rs = db.stmt.executeQuery(query);
        float Theftwtrom = 0;
        while(db.rs.next()){
            Theftwtrom = db.rs.getFloat("count(distinct diefstal.diefstal_voorvalnummer)");
        }
        return Theftwtrom;

    }
    //Alle diefstallen
    //Dit gebruiken we ook om mee te rekenen en kijken hoeveel procent van de diefstallen bij een fietstrommel is
    public float TotalTheft() throws SQLException {
        String query2 = "SELECT count(diefstal.diefstal_voorvalnummer) FROM diefstal";
        db.rs = db.stmt.executeQuery(query2);
        float TotalTheftie = 0;
        while (db.rs.next()) {
            TotalTheftie = db.rs.getFloat("count(diefstal.diefstal_voorvalnummer)");
        }
        return TotalTheftie;
    }
    //Alle fietstrommel locaties
    //Dit doen we om het percentage straten met fietstrommels te berekenen
    public float Streetwtrom() throws SQLException{
        String query3 ="SELECT COUNT(DISTINCT fietstrommels.fiestrommel_locatie) FROM fietstrommels";
        db.rs = db.stmt.executeQuery(query3);
        float Streetwithrom = 0;
        while(db.rs.next()){
            Streetwithrom = db.rs.getFloat("COUNT(DISTINCT fietstrommels.fiestrommel_locatie)");
        }
        return Streetwithrom;
    }

    //Alle locaties zonder fietstrommels
    public float Streetntrom() throws SQLException{
        String query4 ="SELECT COUNT(DISTINCT diefstal.diefstal_straat) FROM diefstal";
        db.rs = db.stmt.executeQuery(query4);
        float Streetnotrom = 0;
        while (db.rs.next()){
            Streetnotrom = db.rs.getFloat("COUNT(DISTINCT diefstal.diefstal_straat)");
        }
        return Streetnotrom;

    }
    //Hoeveel diefstallen in een straat gebeuren met een fietstrommel
    public double PercentTheft() throws SQLException{
        double PercentWtrom;
        PercentWtrom = (TheftTrom() / TotalTheft());
        PercentWtrom = PercentWtrom * 100;
        return PercentWtrom;
    }

    //Berekent het aantal straten waar een fietstrommel is
    public double PercentTrom() throws SQLException{
        double PercentTrommel;
        PercentTrommel = (Streetwtrom() / Streetntrom());
        PercentTrommel = PercentTrommel * 100;
        return PercentTrommel;
    }
}