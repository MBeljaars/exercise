package com.LineChart;

import com.Database;
import com.Nodes.ValueNode;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;

import java.sql.SQLException;
import java.util.ArrayList;


public class Jaarlijkse_diefstallen {
    //This class has a reference to Database and a list 'Blacklist' that will store all values in pairs.
    private Database db;
    private ArrayList<ValueNode> BlackList;

    //Constructor: run query and save it in 'Blacklist' for future reference
    public Jaarlijkse_diefstallen(Database dbin) throws SQLException{
        db = dbin;
        String query ="SELECT diefstal_gemjaar,COUNT(diefstal_gemjaar) FROM diefstal GROUP BY diefstal_gemjaar";
        db.rs = db.stmt.executeQuery(query);
        BlackList = new ArrayList<>();
        while(db.rs.next()){
            BlackList.add(new ValueNode(db.rs.getInt("diefstal_gemjaar"),db.rs.getInt("COUNT(diefstal_gemjaar)")));
        }
    }

    //When this method is called it will pop up a new window and display a graph with data from the 'Blacklist'
    public void showChart() {
        //Initialise the stage and linechart
        Stage stage = new Stage();
        stage.setTitle("Fietsdiefstal in jaren");
        final NumberAxis xAxis = new NumberAxis(2011, 2013, 1);
        final NumberAxis yAxis = new NumberAxis(1500,9500,100);
        xAxis.setLabel("Gegeven jaar");
        yAxis.setLabel("Aantal fietsdiefstallen");
        final LineChart<Number,Number> lineChart =
                new LineChart<>(xAxis,yAxis);

        lineChart.setTitle("Fietsdiefstallen per jaar omgeving Rotterdam");
        XYChart.Series series = new XYChart.Series();
        series.setName("Fietsdiefstallen");

        //Every value in the 'Blacklist' is added to the graph data to show the magic.
        for (ValueNode values: this.BlackList){
            if (values.GetValue() > 180)
            {
                series.getData().add(new XYChart.Data(values.GetPoint(), values.GetValue()));
            }
        }
        //Add the chart to the stage to draw it
        Scene scene  = new Scene(lineChart,800,600);
        lineChart.getData().add(series);
        stage.setScene(scene);
        stage.show();
    }
}