package com.LineChart;

import com.Database;
import com.Nodes.ValueNode;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;

import java.sql.SQLException;
import java.util.ArrayList;


public class Maandelijkse_diefstallen {
    //This class has a reference to Database and 3 lists that will store all values in pairs per year.
    private Database db;
    private ArrayList<ValueNode> Lijst1;
    private ArrayList<ValueNode> Lijst2;
    private ArrayList<ValueNode> Lijst3;
    //Constructor: run query and save it in private list for future reference
    public Maandelijkse_diefstallen(Database dbin) throws SQLException{
        db = dbin;
        String query ="SELECT diefstal_gemjaar,diefstal_gemmaand,count(diefstal_voorvalnummer) FROM diefstal GROUP BY diefstal_gemjaar,diefstal_gemmaand";
        db.rs = db.stmt.executeQuery(query);
        Lijst1 = new ArrayList<>();
        Lijst2 = new ArrayList<>();
        Lijst3 = new ArrayList<>();
        int index = 0;
        while(db.rs.next()){
            if (db.rs.getInt("diefstal_gemjaar") == 2011){
                Lijst1.add(new ValueNode(db.rs.getInt("diefstal_gemmaand"),db.rs.getInt("count(diefstal_voorvalnummer)")));
            }
            if (db.rs.getInt("diefstal_gemjaar") == 2012){

                Lijst2.add(new ValueNode(db.rs.getInt("diefstal_gemmaand"),db.rs.getInt("count(diefstal_voorvalnummer)")));
            }
            if (db.rs.getInt("diefstal_gemjaar") == 2013){
                Lijst3.add(new ValueNode(db.rs.getInt("diefstal_gemmaand"),db.rs.getInt("count(diefstal_voorvalnummer)")));
            }
            index++;
        }
    }

    //When this method is triggered; it will open a new window with graph on stage.
    public void showChart() {
        Stage stage = new Stage();
        stage.setTitle("Fietsdiefstallen per maand");
        final NumberAxis xAxis = new NumberAxis(1,12,1);
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("Maand nummer");
        yAxis.setLabel("Aantal fietsdiefstallen");

        final LineChart<Number,Number> lineChart =
                new LineChart<>(xAxis,yAxis);

        lineChart.setTitle("Fietsdiefstallen per maand omgeving Rotterdam");
        XYChart.Series series = new XYChart.Series();
        series.setName("2011");
        //For every list add a new list for the graph to be shown in said graph.
        for (ValueNode values: this.Lijst1){
                series.getData().add(new XYChart.Data(values.GetPoint(), values.GetValue()));
        }
        XYChart.Series series2 = new XYChart.Series();
        series2.setName("2012");
        for (ValueNode values: this.Lijst2){
                series2.getData().add(new XYChart.Data(values.GetPoint(), values.GetValue()));
        }

        XYChart.Series series3 = new XYChart.Series();
        series3.setName("2013");
        for (ValueNode values: this.Lijst3){
            series3.getData().add(new XYChart.Data(values.GetPoint(), values.GetValue()));
        }
        //Add the chart to the stage to draw it with all it's years
        Scene scene  = new Scene(lineChart,800,600);
        lineChart.getData().addAll(series,series2,series3);
        stage.setScene(scene);
        stage.show();
    }
}